use std::{
    fs,
    io::{prelude::*, BufReader},
    net::{TcpListener, TcpStream},
};

use hello::ThreadPool;

fn main() {
    let listener = TcpListener::bind("127.0.0.1:7878").unwrap();
    let pool = ThreadPool::new(4);

    for stream in listener.incoming().take(2) {
        let stream = stream.unwrap();

        pool.execute(|| {
            handle_connection(stream);
        });
    }

    println!("Shutting down.");
}

fn handle_connection(mut stream: TcpStream) {
    let buf_reader = BufReader::new(&mut stream);
    let request_line = buf_reader.lines().next().unwrap().unwrap();
    let request_parts: Vec<_> = request_line.split_ascii_whitespace().collect();

    let response = match request_parts[..] {
        ["GET", path, "HTTP/1.1"] => {
            let status_line = "HTTP/1.1 200 OK";
            let stripped_path = path.strip_prefix("/").unwrap_or(path);
            let path_with_root = if stripped_path.is_empty() {
                "index.html"
            } else {
                stripped_path
            };
            let contents = fs::read_to_string(path_with_root)
                .or_else(|_| fs::read_to_string("404.html"))
                .unwrap();
            let length = contents.len();

            format!("{status_line}\r\nContent-Length: {length}\r\n\r\n{contents}")
        }
        _ => "HTTP/1.1 501 Not Implemented\r\n\r\n".to_owned(),
    };

    stream.write_all(response.as_bytes()).unwrap();
}
